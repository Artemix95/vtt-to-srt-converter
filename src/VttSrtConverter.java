import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VttSrtConverter {
    public static void main(String[] args) {
        String firstInput;
        String input;

        Scanner first = new Scanner(System.in);
        System.out.println("Sono già numerati? (s/n) ");
        firstInput = first.nextLine();

        switch (firstInput) {
            case "s" -> {
                do {

                    // input file from user
                    Scanner in = new Scanner(System.in);
                    System.out.println("What is the filename? ");
                    input = in.nextLine();
                    // check if there are quotation marks (start end), and removes it.
                    if ((input.charAt(0) == '"') && (input.charAt(input.length() - 1) == '"')) {
                        input = input.substring(1, input.length() - 1);
                    } else if ((input.charAt(0) == '\'') && (input.charAt(input.length() - 2) == '\'')
                            && (input.charAt(input.length() - 1) == ' ')) {
                        input = input.substring(1, input.length() - 2);
                    }

                    // copy all lines of the file in a List
                    ArrayList<String> lines = new ArrayList<>();
                    try {
                        BufferedReader reader = new BufferedReader(
                                new InputStreamReader(new FileInputStream(input), StandardCharsets.UTF_8));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            lines.add(line);
                        }
                    } catch (FileNotFoundException e) {
                        System.out.println("File not found");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    clearTop(lines);

                    ArrayList<String> clearedLines;
                    clearedLines = clearLines(removeEmptyLines(lines));

                    String[] tempArrayLines = new String[clearedLines.size()];
                    tempArrayLines = clearedLines.toArray(tempArrayLines);

                    ArrayList<String> joinedLines;
                    joinedLines = joinLines(tempArrayLines);

                    // Set new name for output converted file
                    String newFileName = input;
                    if (input.contains(".vtt")) {
                        newFileName = input.replace(".vtt", ".srt");
                    }
                    // create a new file and check if the name already exists
                    File f = new File(newFileName);
                    if (f.exists() && !f.isDirectory()) {
                        System.out.println("The file already exists, delete it and try again");

                    } else {
                        // write down the new clearedList
                        try {
                            BufferedWriter convertedFile = Files.newBufferedWriter(Paths.get(newFileName));
                            for (String s : joinedLines) {
                                convertedFile.write(s + "\n");
                            }
                            convertedFile.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                } while (true);
            }

            case "n" -> {
                do {

                    // input file from user
                    Scanner in = new Scanner(System.in);
                    System.out.println("What is the filename? ");
                    input = in.nextLine();
                    // check if there are quotation marks (start end), and removes it.
                    if ((input.charAt(0) == '"') && (input.charAt(input.length() - 1) == '"')) {
                        input = input.substring(1, input.length() - 1);
                    } else if ((input.charAt(0) == '\'') && (input.charAt(input.length() - 2) == '\'')
                            && (input.charAt(input.length() - 1) == ' ')) {
                        input = input.substring(1, input.length() - 2);
                    }

                    // copy all lines of the file in a List
                    ArrayList<String> lines = new ArrayList<>();
                    try {
                        BufferedReader reader = new BufferedReader(
                                new InputStreamReader(new FileInputStream(input), StandardCharsets.UTF_8));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            lines.add(line);
                        }
                    } catch (FileNotFoundException e) {
                        System.out.println("File not found");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    clearTop(lines);

                    ArrayList<String> clearedLines;
                    clearedLines = addNumbers(clearTimeLines(clearLines(lines)));

                    // Set new name for output converted file
                    String newFileName = input;
                    if (input.contains(".vtt")) {
                        newFileName = input.replace(".vtt", ".srt");
                    }
                    // create a new file and check if the name already exists
                    File f = new File(newFileName);
                    if (f.exists() && !f.isDirectory()) {
                        System.out.println("The file already exists, delete it and try again");

                    } else {
                        // write down the new clearedList
                        try {
                            BufferedWriter convertedFile = Files.newBufferedWriter(Paths.get(newFileName));
                            for (String s : clearedLines) {
                                convertedFile.write(s + "\n");
                            }
                            convertedFile.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                } while (true);

            }
        }

    }

    // elimina le righe iniziali dei VTT
    public static void clearTop(ArrayList<String> lines) {
        int lastLineToRemove = 0;
        for (String s : lines) {
            if ((s.equals("1")) || (s.startsWith("00"))) {
                break;
            }
            lastLineToRemove += 1;
        }
        lines.subList(0, lastLineToRemove).clear();
    }

    // elimino tutte le righe vuote
    public static ArrayList<String> removeEmptyLines(ArrayList<String> list) {
        ArrayList<String> toDelete = new ArrayList<>();
        for (String s2 : list) {
            if (s2.isEmpty()) {
                toDelete.add(s2);
            }
        }
        list.removeAll(toDelete);
        return list;
    }

    // elimino tutte le scritte obsolete dei vtt
    public static ArrayList<String> clearLines(ArrayList<String> lines) {
        ArrayList<String> clearedLines = new ArrayList<>();
        for (String s : lines) {
            // delete unwanted strings from the lines and convert . in ,
            if (s.contains("<c.white><c.mono_sans>") || s.contains("</c.mono_sans></c.white>")) {
                String clearLine = s.replaceAll("<c.white><c.mono_sans>", "");
                String clearLine2 = clearLine.replaceAll("</c.mono_sans></c.white>", "");
                clearedLines.add(clearLine2);
            } else if (s.contains(" position:")) {
                String[] splitString = s.split(" position:");
                String dotToComma = splitString[0].replace('.', ',');
                clearedLines.add(dotToComma);
            } else if (s.contains("</c.bg_transparent>") || s.contains("<c.bg_transparent>")) {
                String clearLine = s.replaceAll("</c.bg_transparent>", "");
                String clearLine2 = clearLine.replaceAll("<c.bg_transparent>", "");
                clearedLines.add(clearLine2);
            } else if ((s.contains("<c.bg_black><c.default>") || s.contains("</c.default></c.bg_black>"))) {
                String clearLine = s.replaceAll("<c.bg_black><c.default>", "");
                String clearLine2 = clearLine.replaceAll("</c.default></c.bg_black>", "");
                clearedLines.add(clearLine2);
            } else if ((s.contains("<c.speakerTop>") || s.contains("</c>") || s.contains("<c.speaker>"))) {
                String clearLine = s.replaceAll("<c.speakerTop>", "");
                String clearLine2 = clearLine.replaceAll("</c>", "");
                String clearLine3 = clearLine2.replaceAll("<c.speaker>", "");
                clearedLines.add(clearLine3);
            } else {
                clearedLines.add(s);
            }

        }
        return clearedLines;
    }

    // pulisce le righe che segnano il tempo dei sottotitoli
    // 00:00:02.602 --> 00:00:04.904 line:90%,end
    public static ArrayList<String> clearTimeLines(ArrayList<String> list) {
        Pattern time = Pattern.compile("\\d{2}:\\d{2}:\\d{2}\\.\\d{3}");
        ArrayList<String> clearedTimeLines = new ArrayList<>();
        for (String s : list) {
            Matcher matcher = time.matcher(s);
            if (matcher.find()) {
                String[] splitString = s.split("\\s-->\\s");
                String startTime = splitString[0].replaceAll("\\s*line.*", "").trim();
                String endTime = splitString[1].replaceAll("\\s*line.*", "").trim();
                clearedTimeLines.add(startTime + " --> " + endTime);
            } else {
                clearedTimeLines.add(s);
            }
        }
        return clearedTimeLines;
    }

    // converte il formato del tempo da HH:MM:SS.mm a HH:MM:SS,mm
    public static String convertTimeFormat(String time) {
        String[] parts = time.split(":");
        String seconds = parts[2].replace(".", ",");
        return parts[0] + ":" + parts[1] + ":" + seconds;
    }

    // unisce insieme i sottotitoli che hanno lo stesso tempo
    public static ArrayList<String> joinLines(String[] list) {
        Pattern doubleDigits = Pattern.compile("\\d{2}:\\d{2}");

        for (int i = 6; i < list.length; i++) {
            Matcher matcher = doubleDigits.matcher(list[i]);
            if ((list[i].equals(list[i - 3])) && (list[i].equals(list[i - 6])) && (matcher.find())) {
                list[i - 4] = list[i - 2];
                list[i - 3] = list[i + 1];
                list[i - 2] = "";
                list[i - 1] = "";
                list[i] = "";
                list[i + 1] = "";

            } else if ((list[i].equals(list[i - 3])) && (matcher.find())) {
                list[i - 1] = list[i + 1];
                list[i + 1] = "";
                list[i] = "";
            }
        }
        ArrayList<String> joinedLines = new ArrayList<>(Arrays.asList(list));

        // sistema la numerazione dei sottotioli e aggiunge una newline tra l'uno e
        // l'altro
        int number = 2;
        for (int i = 1; i < joinedLines.size(); i++) {
            if (joinedLines.get(i).matches("\\d") ||
                    (joinedLines.get(i).matches("\\d{2}")) ||
                    (joinedLines.get(i).matches("\\d{3}")) ||
                    (joinedLines.get(i).matches("\\d{4}"))) {
                joinedLines.add(i, "\n");
                joinedLines.set(i + 1, String.valueOf(number));
                i++;
                number++;
            }
        }

        return removeEmptyLines(joinedLines);
    }

    // aggiunge la numerazione ai sottotitoli
    public static ArrayList<String> addNumbers(ArrayList<String> list) {
        Pattern time = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
        list.add(0, "1");
        int number = 1;
        for (int i = 1; i < list.size(); i++) {
            Matcher matcher = time.matcher(list.get(i));
            if (matcher.find()) {

                list.set(i - 1, String.valueOf(number));

                i++;
                number++;
            }
        }

        // aggiunge una newline tra un sottotitolo e l'altro
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).matches("\\d") ||
                    (list.get(i).matches("\\d{2}")) ||
                    (list.get(i).matches("\\d{3}")) ||
                    (list.get(i).matches("\\d{4}"))) {
                list.add(i, "\n");

                i++;

            }
        }

        return list;
    }
}